# Penjelasan struktur folder

1. css adalah folder yang digunakan untuk menyimpan file style.css
2. pijarcamp.sql adalah file database yang digunakan
3. function.php adalah digunakan untuk menyimpan fungsi yang telah di deklarasikan
4. index.php adalah file utama yang digunakan untuk menampilkan data pada databse
5. hapus.php adalah file yang berfungsi untuk menghapus data pada database
6. tambah.php adalah file yang berfungsi untuk menginput/menambahkan data pada database
7. ubah.php adalah file yang berfungsi untuk mengubah data pada database
8. readme adalah file readme ini
9. style.css adalah file yang berfungsi untuk mengubah tampilan pada aplikasi

# Sumber daya yang digunakan

1. Bootstrap 5
2. PHP
3. Mysql
