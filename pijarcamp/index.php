<?php
require 'function.php';
$distro = query("SELECT * FROM produk");
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="" href="css/style.css">

    <title>Mendadak Distro</title>
</head>

<body>
    <div class="container w-75 p-3">
        <h1>Mendadak Distro</h1>

        <div class="row  text-end">
            <div class="col mx-5">
                <a class="btn btn-primary btn-sm" href="tambah.php" role="button">Tambah</a>
            </div>
        </div>
        <div class="row text-center p-3">
            <div class="col">
                <table class="table">
                    <thead class="table-dark">
                        <tr>
                            <td>No.</td>
                            <td>Nama Produk</td>
                            <td>Keterangan</td>
                            <td>Harga</td>
                            <td>Jumlah Unit</td>
                            <td></td>
                        </tr>
                    </thead>
                    <?php $i = 1; ?>
                    <?php foreach ($distro as $row) : ?>
                    <tbody>
                        <td><?= $i ?></td>
                        <td><?= $row['nama_produk'] ?></td>
                        <td><?= $row['keterangan'] ?></td>
                        <td>Rp <?= $row['harga'] ?></td>
                        <td><?= $row['jumlah'] ?></td>
                        <td>
                            <a class="btn btn-success btn-sm" href="ubah.php?id=<?= $row['id'] ?>" id='id'>Ubah</a>
                            <a class="btn btn-danger btn-sm" href="hapus.php?id=<?= $row['id'] ?>"
                                onclick="return confirm('yakin?')" id='id'>Hapus</a>
                        </td>
                    </tbody>
                    <?php $i++; ?>
                    <?php endforeach ?>
                </table>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>

</html>